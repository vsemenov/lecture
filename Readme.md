
# TA Contacts

* Sergio (Head TA): <martiser@ethz.ch>
* Lucas:            <amlucas@ethz.ch>
* Athena:           <eceva@ethz.ch>
* Petr:             <kpetr@ethz.ch>
* Guido:            <novatig@ethz.ch>
* Pantelis:         <pvlachas@ethz.ch>
* Fabian:           <fabianw@mavt.ethz.ch>

# Exercise Hand-in

Please submit your exercise solutions via Moodle (nethz login required)

https://moodle-app2.let.ethz.ch/course/view.php?id=11566

# Lecture Schedule

| When       | Who | What                                                  |
|------------|-----|-------------------------------------------------------|
| 2019-09-20 | PK  | _Introduction, Architectures, Performance_            |
| 2019-09-27 | FW  | _Shared Memory, Concurrency, Threads_                 |
| 2019-10-04 | FW  | _OPEN-MP 1 (part 1)_                                  |
| 2019-10-11 | FW  | _OPEN-MP 2 (part 2), Linear Algebra: BLAS_            |
| 2019-10-18 | PK  | _SVD and Neural Networks_                             |
| 2019-10-25 | PK  | _Neural Networks and Matrix-Vector Multiplication_    |
| 2019-11-01 | LA  | _Instruction Set Architecture and Pipelining_         |
| 2019-11-08 | LA  | _MPI 1 (part 1)_                                      |
| 2019-11-15 | LA  | _MPI 2 (part 2)_                                      |
| 2019-11-22 | LA  | _MPI 3 (part 3) and Strong/Weak Scaling_              |
| 2019-11-29 | PK  | _Structured Grids and Finite Differences (Diffusion)_ |
| 2019-12-06 | PK  | _Particles and N-Body Solvers (Diffusion)_            |
| 2019-12-13 | FW  | _Vectorization_                                       |
| 2019-12-20 | PK  | _Particle-Grid Algorithms (cell-lists, remeshing)_    |

# Exam

## Allowed Documents

You are allowed to bring a **handwritten** summary of 4 A4 sheets, written on
the front and back pages (8 pages total).  In addition, we will provide you
with the following material:

1. All lecture slides
2. All exercise solutions
3. Lecture notes that are distributed in addition to the slides
4. Manuals / references:
    * OpenMP 4.5 specification
    * OpenMP 4.5 reference card
    * MPI 3.1 standard
    * MPI reference card
    * cppreference webpage (offline version)
    * Intel intrinsics guide (offline version)
    * The book by Victor Eijkhout

# Frequently Asked Questions (FAQ)

Please have a look at these [Frequently asked questions](./faq/faq.md)
