// File       : benchmark_1Ddiffusion.cpp
// Description: Benchmark 1D diffusion test
// Copyright 2019 ETH Zurich. All Rights Reserved.
#include <iostream>
#include <vector>
#include <chrono> // use std::chrono to benchmark your code

class CacheFlusher
{
public:
    using Size = long long int;
    CacheFlusher(Size size = 1<<26 /* 64 MB */) :
        size(size),
        buf(new volatile unsigned char[size])
    {}

    ~CacheFlusher()
    {
        delete[] buf;
    }

    void flush()
    {
        for (Size i = 0; i < size; ++i)
            buf[i] += i;
    }
    
private:
    const Size size {0};
    volatile unsigned char *buf;
};

int main()
{
    // convenient tool to flush the cache
    // use it as cacheFlusher.flush()
    CacheFlusher cacheFlusher;

    // <your code here>
    // ...
    
    return 0;
}
