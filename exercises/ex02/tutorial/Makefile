# Makefile consists of rules:
# target: prerequisites
# 	cmd1
# 	cmd2
# 	...

run: main
	OMP_NUM_THREADS=4 ./main

run_race: main
	OMP_NUM_THREADS=1000 ./main | sort | uniq -d
	@#              |
	@#              | oversubscribing threads (1000 > 4)
	@#              | will spawn 1000 threads on the node,
	@#              | use only for quick tests,
	@#              | will interfere with other running jobs
	@#              |_______________________________________

main: main.cpp Makefile
	g++ -std=c++11 main.cpp -fopenmp -o main

submit:
	OMP_NUM_THREADS=4 bsub -n 4 -R "span[ptile=4]" -W 10 ./main
	@#              |         |                |      |
	@#              |         |                |      |
	@#              |______   |                |      | wall-time in minutes
	@#                     |  |                |      |_____________________
	@#                     |  |                |
	@#                     |  |                |
	@#                     |  |                |
	@# number of threads   |  |                |
	@# for OpenMP          |  |                | number of cores on each node
	@# (must be less       |  |                | (with '-n 4' makes the scheduler
	@# or equal 4*2        |  |                | give one node with 4 cores)
	@# with '-n 4' ,       |  |                |_________________________________
	@# physical cores      |  |
	@# plus hyprethreading |  |
	@# ____________________|  |
	@#                        |
	@#                        |
	@#                        | total number of cores
	@#                        | (can be on different nodes)
	@#                        |____________________________

interactive:
	@# gives you a shell on a compute node,
	@# you can launch your executable multiple times interactively,
	@# useful for debugging
	OMP_NUM_THREADS=4 bsub -n 4 -R "span[ptile=4]" -W 10 -Is /bin/bash


# list of targets that do not correspond to actual files,
# executed unconditionally
# even if a file with this name exists and not older than prerequisites
.PHONY: run run_oversub submit interactive
