#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <chrono>
#include <cblas.h>

int main(int argc, char** argv)
{
  size_t N = 2 << 13;

  std::vector<double> A(N*N);
  std::vector<double> b(N);

  std::vector<double> manualResult(N);
  std::vector<double> blasResult(N);

  for (size_t i = 0; i < N; i++)
  {
   b[i] = (double)i / (double)N;
   for (size_t j = 0; j < N; j++) A[i*N + j] = (double)(i+j) / (double)N;
  }

  auto manualStartTime = std::chrono::system_clock::now();

  for (size_t i = 0; i < N; i++)
   for (size_t j = 0; j < N; j++)
    manualResult[i] += b[i] * A[j*N + i];

  double manualTime = std::chrono::duration<double>(std::chrono::system_clock::now()-manualStartTime).count();

  auto blasStartTime = std::chrono::system_clock::now();

  /********************************************************************************
  * Your BLAS solver code goes here.
  ********************************************************************************/


  /********************************************************************************
  * End of BLAS solver.
  ********************************************************************************/

  double blasTime = std::chrono::duration<double>(std::chrono::system_clock::now()-blasStartTime).count();

  size_t flopCount = N*N*2; // One multiplication and one sum = 2 flops
  size_t byteCount = N*N*4*sizeof(double); // Three reads and one write = 4 mem accesses
  double arithmeticIntensity = (double)flopCount / (double) byteCount;

  double manualChecksum = 0.0;
  double blasChecksum = 0.0;
  for (size_t i = 0; i < N; i++)
  {
   manualChecksum += manualResult[i];
   blasChecksum += blasResult[i];
  }

  printf("--------------------------------------------------------------\n");
  printf("Execution Statistics\n");
  printf("--------------------------------------------------------------\n");
  printf("Vector Size: %lu\n", N);
  printf("MFlops Executed: %.2f\n", (double)flopCount / (1024.0 * 1024.0));
  printf("MBytes Accessed: %.2f\n", (double)byteCount / (1024.0 * 1024.0));
  printf("Arithmetic Intensity: %f\n", arithmeticIntensity);
  printf("\n");
  printf("Manual Variant:\n");
  printf(" + Checksum: %f\n", manualChecksum);
  printf(" + Computation Time: %.3fs\n", manualTime);
  printf(" + GFlops/s:  %f\n",  flopCount / (manualTime * 1024.0 * 1024.0 * 1024.0));

  if (blasChecksum > 1.0)
  {
   printf("\n");
   printf("BLAS Variant:\n");
   printf(" + Checksum: %f\n", blasChecksum);
   printf(" + Computation Time: %.3fs\n", blasTime);
   printf(" + GFlops/s:  %f\n",  flopCount / (blasTime * 1024.0 * 1024.0 * 1024.0));
   printf("\n");
   printf("Performance Ratio: %.2f%%\n", (blasTime/manualTime)*100.0);
  }
  else printf("CBLAS Variant not yet implemented.\n");

  return 0;
}

 
