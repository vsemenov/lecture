#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <chrono>
#include <cblas.h>

int main(int argc, char** argv)
{
  size_t N = 2 << 24;
  size_t iterations = 64;

  double manualResult = 0.0;
  double blasResult = 0.0;

  std::vector<double> vA(N);
  std::vector<double> vB(N);

  for (size_t i = 0; i < N; i++)
  {
   vA[i] = (double)i / (double)N;
   vB[i] = (double)i / (double)N;
  }

  auto manualStartTime = std::chrono::system_clock::now();

  for (size_t iter = 0; iter < iterations; iter++)
  {
   manualResult = 0.0;
   for (size_t i = 0; i < N; i++) manualResult += vA[i] * vB[i];
  }

  double manualTime = std::chrono::duration<double>(std::chrono::system_clock::now()-manualStartTime).count();

  auto blasStartTime = std::chrono::system_clock::now();

  /********************************************************************************
  * Your BLAS solver code goes here.
  ********************************************************************************/

  for (size_t iter = 0; iter < iterations; iter++)
   blasResult = cblas_ddot(N, vA.data(), 1, vB.data(), 1);

  /********************************************************************************
  * End of BLAS solver.
  ********************************************************************************/

  double blasTime = std::chrono::duration<double>(std::chrono::system_clock::now()-blasStartTime).count();

  size_t flopCount = N*iterations*2; // One multiplication and one sum = 2 flops
  size_t byteCount = N*iterations*4*sizeof(double); // Three reads and one write = 4 mem accesses
  double arithmeticIntensity = (double)flopCount / (double) byteCount;

  printf("--------------------------------------------------------------\n");
  printf("Execution Statistics\n");
  printf("--------------------------------------------------------------\n");
  printf("Vector Size: %lu\n", N);
  printf("MFlops Executed: %.2f\n", (double)flopCount / (1024.0 * 1024.0));
  printf("MBytes Accessed: %.2f\n", (double)byteCount / (1024.0 * 1024.0));
  printf("Arithmetic Intensity: %f\n", arithmeticIntensity);
  printf("\n");
  printf("Manual Variant:\n");
  printf(" + Result: %f\n", manualResult);
  printf(" + Computation Time: %.3fs\n", manualTime);
  printf(" + GFlops/s:  %f\n",  flopCount / (manualTime * 1024.0 * 1024.0 * 1024.0));

  if (blasResult > 1.0)
  {
   printf("\n");
   printf("BLAS Variant:\n");
   printf(" + Result: %f\n", blasResult);
   printf(" + Computation Time: %.3fs\n", blasTime);
   printf(" + GFlops/s:  %f\n",  flopCount / (blasTime * 1024.0 * 1024.0 * 1024.0));
   printf("\n");
   printf("Performance Ratio: %.2f%%\n", (blasTime/manualTime)*100.0);
  }
  else printf("CBLAS Variant not yet implemented.\n");

  return 0;
}

 
