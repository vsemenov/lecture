# Purpose

Understand basic compiler/linker flags to work with external libraries

## Examples

Please read the comments in these files for more detailed explanations.

```
.
├── local_lib
│   ├── libtest
│   │   ├── include
│   │   │   └── test.h
│   │   ├── lib
│   │   └── src
│   │       └── libtest.cpp
│   ├── Makefile
│   └── myapp.cpp
├── Readme.md
└── system_lib
    ├── Makefile
    └── myapp.cpp
```

### Basic stages when compiling a program

1. Preprocessing
2. Compilation
3. Assembly
4. Linking

[See this post for more details.](https://www.calleerlandsson.com/the-four-stages-of-compiling-a-c-program/)

The input/output between these stages is as follows:

```text
Source code          -> [Preprocessing] -> Source code
Source code          -> [Compilation]   -> Assembly program
Assembly program     -> [Assembly]      -> Machine instructions
Machine instructions -> [Linking]       -> Executable code
```

### Local library (not installed in a system directory)

The code for this example is located in `local_lib`.  The code builds a small
library that provides a header file (its application programming interface
[API]) and compiled code which we need to make the linker aware of.  Here we
link to our own library `libtest.a`.

### System library (installed in a system directory)

The code for this example is located in `system_lib`.  The code calls two
routines from the level 1 BLAS library.  To do so we link to a system wide
cblas library installation.  Here we will link to a shared system library
`libcblas.so`.
