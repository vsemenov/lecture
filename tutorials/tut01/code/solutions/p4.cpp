#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <chrono>

int main(int argc, char** argv)
{
  size_t N = 1024;

  // Allocating a square matrix
  std::vector<double> A(N*N);
  std::vector<double> B(N*N);
  std::vector<double> C(N*N);

  // Initializing the matrix's vales
  for (size_t i = 0; i < N; i++)
   for (size_t j = 0; j < N; j++)
   {
    A[i*N + j] = 1.1*i+j;
    B[i*N + j] = 1.05*i+j;
    C[i*N + j] = 0.0;
   }

  auto startTime = std::chrono::system_clock::now();

  // Calculating A*B
  for (size_t i = 0; i < N; i++)
   for (size_t j = 0; j < N; j++)
    for (size_t k = 0; k < N; k++)
     C[i*N + j] += A[i*N + k] * B[k*N + j];

  auto endTime = std::chrono::system_clock::now();

  // Printing Checksum & Time
  double checksum = 0;
  for (size_t i = 0; i < N; i++)
   for (size_t j = 0; j < N; j++)
    checksum += C[i*N + j];


  printf("Finished. Checksum: %f, Time: %.8fs\n", checksum, std::chrono::duration<double>(endTime-startTime).count());

  return 0;
}

 

