import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

# if __name__ == "__main__":

# mean = [0,0]
# mean = [1,3]
# cov = [[1,1.2],[1.2,3]]

mean = [0,0]
cov = [[1,1.2],[1.2,3]]


data = np.random.multivariate_normal(mean, cov, 1024)
data = np.array(data)
# print(data)
print(np.shape(data))


plt.plot(data[:,0], data[:,1], "x")
DD = 10
plt.xlim(mean[0]-DD,mean[0]+DD)
plt.ylim(mean[1]-DD,mean[1]+DD)
plt.xlabel("$x^1$")
plt.ylabel("$x^2$")
plt.savefig("figures/data_2D.pdf")
plt.show()

np.savetxt("2D_dataset.txt", data, delimiter=',', newline='\r\n')
del data

data = np.loadtxt("2D_dataset.txt", delimiter=',')
print(np.shape(data))

