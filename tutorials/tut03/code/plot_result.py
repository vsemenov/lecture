import numpy as np
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os
from sklearn.decomposition import PCA


# if __name__ == "__main__":

#   # SELECT YOUR METHOD:
#   # method = "PCA_PYTHON" # REFERENCE SOLUTION (COMPARE YOUR CODE WITH THIS)
#   method = "OJA" # YOUR CPP IMPLEMENTATION

for method in ["PCA_PYTHON", "OJA", "NN"]:
  print("###############################################")
  print("METHOD = {:}".format(method))
  # SELECT THE DATASET
  dataset = "2D"

  DIM = 2
  n_components=2

  data = np.loadtxt("./data/{:}_dataset.txt".format(dataset), delimiter=',')
  N, D = np.shape(data)
  data_mean_true = np.mean(data,axis=0)
  data_std_true = np.std(data,axis=0)
  data_centered = data - data_mean_true

  # data_centered = data_centered/data_std_true


  eig_true = np.loadtxt("./results/{:}_eigs_true.txt".format(dataset))
  print("TRUE EIGENVALUES:")
  print(eig_true)

  # LOADING THE RESULTS
  result_str = "./results/{:}{:}_components.txt".format(dataset, method)
  pca_components = np.loadtxt(result_str, delimiter=',')

  temp = list(np.shape(pca_components))
  size = 1
  for s in temp:
    size = size*s
  size = int(size)
  n_components = int(size/D)
  pca_components = np.reshape(pca_components, (n_components, -1))
  result_str = "./results/{:}{:}_mean.txt".format(dataset, method)
  data_mean = np.loadtxt(result_str, delimiter=',')
  result_str = "./results/{:}{:}_std.txt".format(dataset, method)
  eig_pred = np.loadtxt("./results/{:}{:}_eig.txt".format(dataset, method), delimiter=',')
  data_std = np.loadtxt("./results/{:}{:}_std.txt".format(dataset, method), delimiter=',')


  print("PREDICTED EIGENVALUES:")
  print(eig_pred)

  print("PREDICTED COMPONENTS:")
  print(pca_components)

  for pca_comp in pca_components:
    # For illustration purposes, we additionally scaled them with a single float (the standard deviation of the whole dataset)
    # scale = 3*data_centered.std()
    scale = 4

    pca_comp = pca_comp * scale
    plt.arrow(0, 0, pca_comp[0], pca_comp[1], color="red", linewidth=3, head_width=0.15, head_length=0.2, alpha=0.75, zorder=1)

  plt.xlim(-8, 8)
  plt.ylim(-8, 8)
  plt.plot(data_centered[:,0], data_centered[:,1], "x", color="forestgreen", zorder=0)
  # plt.title("Method: {:} Eigenmodes".format(method))
  plt.savefig("figures/{:}_{:}_components.pdf".format(dataset, method))
  plt.close()





