import numpy as np
import matplotlib
# LOAD THIS IF YOU RUN ON A CLUSTER
# matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os
from itertools import cycle
import scipy

import math, matplotlib.patches as patches
from matplotlib import animation

import torch
import torch.nn as nn

torch.set_default_tensor_type(torch.DoubleTensor)

method = "NN"

dataset = "2D"

data = np.loadtxt("./data/{:}_dataset.txt".format(dataset), delimiter=',')

N, D = np.shape(data)

K = 2

data_mean = np.mean(data,axis=0)
data_std = np.std(data,axis=0)
data_centered = data - data_mean

data_cycl = cycle(data)

beta=1e-4
iter_max = 5000
plt_every = 100
normalize_every = 1000
lambda_ = torch.tensor(0.000) # 0.0001


# weight_init=np.eye(K,D)
# print(weight_init)
weight_init = np.random.randn(K,D)
# weight_init = scipy.linalg.orth(weight_init)


class Perceptron(nn.Module):
  def __init__(self, weight_init):
    super(Perceptron, self).__init__()
    self.K = np.shape(weight_init)[0]
    weight_init = torch.tensor(weight_init)
    self.weight = nn.Parameter(weight_init)

  def forward(self, x):
    z = torch.mm(self.weight, x)
    y = torch.mm(self.weight.T, z)
    return y

  def printNorms(self):
    for k in range(self.K):
      w_k = self.weight[k].data.numpy()
      print("NORMS: |W|= {:}".format(np.linalg.norm(w_k)))

  def normalize(self):
    W = self.weight.detach().data.numpy()
    for k in range(self.K):
      W[k] = W[k] / np.linalg.norm(W[k])
    W = torch.tensor(W)
    self.weight = nn.Parameter(W)

  def regularizationLoss(self):
    l2_reg = torch.tensor(0.)
    for param in self.parameters():
        l2_reg += torch.norm(param)
    return l2_reg

def xToTensor(x):
  x=torch.tensor(x)
  x=torch.reshape(x,(D,1))
  return x

perceptron = Perceptron(weight_init)




# PLOTTING
# Create figure
fig = plt.figure()    
ax = fig.gca()
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_ylim(-8,8)
ax.set_xlim(-8,8)
plt.gca().set_aspect('equal', adjustable='box')
plt.plot(data[:,0], data[:,1], "x", color="forestgreen", zorder=0)
# plt.show()





for iter_ in range(iter_max):
  perceptron.zero_grad()
  print("Iter {:}/{:}, {:}%".format(iter_, iter_max, iter_/iter_max*100.0))
  x = next(data_cycl)
  x = xToTensor(x)
  y = perceptron(x)
  y_target = x

  # print(y.shape)
  # print(y_target.shape)

  loss = torch.pow(y_target-y, 2.0)
  loss = torch.mean(loss)
  loss += lambda_ * perceptron.regularizationLoss()
  print(perceptron.regularizationLoss())

  # print('weigth.grad before backward')
  # print(perceptron.weight.grad)

  loss.backward()

  # print('weigth.grad after backward')
  # print(perceptron.weight.grad)


  # print("Update:")
  for name, param in perceptron.named_parameters():
    # print("Parameter name: {:}".format(name))
    # SIMPLE GRADIENT LEARNING RULE
    # print(perceptron.weight)
    # print('weigth.grad after backward')
    param.data.sub_(param.grad.data * beta)
    # print(perceptron.weight)

  # print(ark)

  perceptron.printNorms()
  if iter_ % normalize_every ==0:
    perceptron.normalize()
    # perceptron.printNorms()
    # print(ark)

  if iter_ % plt_every ==0:
    W = perceptron.weight.detach().numpy()
    plt.clf()
    plt.plot(data[:,0], data[:,1], "x", color="forestgreen", zorder=0)
    for k in range(K):
      plt.arrow(0, 0, W[k, 0], W[k, 1], linewidth=3, head_width=0.15, head_length=0.2, alpha=0.75, zorder=1) #, color="red"
    plt.pause(0.05)


plt.close()

perceptron.printNorms()
W = perceptron.weight.detach().numpy()

# NORMALIZE
for k in range(K):
  W[k] = W[k] / np.linalg.norm(W[k])
print(W)

if k>1:
  print(W[0].T @ W[1])

# data = torch.tensor(data)
# Y = perceptron(data.T).detach().numpy()
# print(np.shape(Y))
# [0.44051275 3.81556062]
#K=1 gives 3.81183218
#K=2 gives [3.81183218 0.44013902]
eig_pred = [0,0]
# eig_pred = np.var(Y, axis=1)
# print(np.shape(eig_pred))
# print(eig_pred)


pca_components=W

np.savetxt("./results/{:}{:}_components.txt".format(dataset, method), pca_components, delimiter=',')
np.savetxt("./results/{:}{:}_mean.txt".format(dataset, method), data_mean, delimiter=',')
np.savetxt("./results/{:}{:}_std.txt".format(dataset, method), data_std, delimiter=',')
np.savetxt("./results/{:}{:}_eig.txt".format(dataset, method), eig_pred, delimiter=',')





