#include <cmath>
#include <iostream>
#include <omp.h>

double func1(const int n)
{
    double sum = 0;
    const double fac = 1.0 / n;
    for (int i = 1; i < n + 1; ++i) {
        sum += std::sqrt(i * fac);
    }
    return sum;
}

double func2(const int n)
{
    double sum = 0;
    const double fac = 1.0 / n;
    for (int i = 0; i < n; ++i) {
        sum += std::exp(-i * fac);
    }
    return sum;
}

int main(void)
{
    double sum = 0;
#pragma omp parallel num_threads(2) reduction(+ : sum)
    {
#pragma omp sections
        {
#pragma omp section
            for (int i = 0; i < 50; ++i) {
                sum += func1(10000000);
            }
#pragma omp section
            for (int i = 0; i < 50; ++i) {
                sum += func2(10000000);
            }
        }
    }
    std::cout << "sum = " << sum << std::endl;
    return 0;
}
