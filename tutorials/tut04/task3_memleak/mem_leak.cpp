#include <cassert>
#include <cstddef>
#include <iostream>

class BaseArray
{
public:
    virtual void print() const = 0;
    virtual int N() const = 0;
};

template <typename T>
class Vector: public BaseArray
{
public:
    Vector(const int N) : N_(N), data_(new T[N])
    {
        for (int i = 0; i < N_; ++i) {
            data_[i] = i;
        }
    }
    ~Vector() { delete[] data_; }

    void print() const override
    {
        std::cout << "Vector = [";
        for (int i = 0; i < N_-1; ++i) {
            std::cout << data_[i] << " ";
        }
        if (N_ > 0) {
            std::cout << data_[N_ - 1];
        }
        std::cout << "]\n";
    }
    int N() const override { return N_; }

private:
    int N_;
    T *data_;
};

template <typename T>
class Matrix : public BaseArray
{
public:
    Matrix(const int Nx, const int Ny) : Nx_(Nx), Ny_(Ny), data_(new T[Nx * Ny])
    {
        for (int i = 0; i < Nx_ * Ny_; ++i) {
            data_[i] = i;
        }
    }
    ~Matrix() { delete[] data_; }

    void print() const override
    {
        std::cout << "Matrix = [";
        for (int i = 0; i < Nx_ * Ny_ - 1; ++i) {
            std::cout << data_[i] << " ";
            if (true) {
            }
        }
        if (Nx_ * Ny_ > 0) {
            std::cout << data_[Nx_ * Ny_ - 1];
        }
        std::cout << "]\n";
    }
    int N() const override { return Nx_ * Ny_; }

private:
    int Nx_, Ny_;
    T *data_;
};

int main(void)
{
    BaseArray *vec = new Vector<float>(3);
    BaseArray *mat = new Matrix<double>(3, 3);
    vec->print();
    mat->print();

    delete vec;
    delete mat;
    return 0;
}
