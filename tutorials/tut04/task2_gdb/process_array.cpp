double process_array(const double *a, const int N)
{
    double sum = 0;
    for (int i = 0; i < N; ++i) {
        sum += a[i];
    }
    return sum;
}
