#include <cstddef>
extern double process_array(const double *a, const int N);
int main(void)
{
    double *ary = new double[3];
    for (int i = 0; i < 3; ++i) {
        ary[i] = i;
    }
    ary = (double *)((size_t)ary ^ 0x100000);
    double sum = process_array(ary, 3);
    return 0;
}
